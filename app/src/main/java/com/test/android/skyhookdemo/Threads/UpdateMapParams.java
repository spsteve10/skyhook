package com.test.android.skyhookdemo.Threads;

import com.test.android.skyhookdemo.Database.AddressDao;
import com.test.android.skyhookdemo.Database.MapSettings;
import com.test.android.skyhookdemo.UI.MainMap.IViewModelUIUpdates;

public class UpdateMapParams {
    AddressDao addressDao;
    IViewModelUIUpdates vmUIUpdate;
    MapSettings mapSettings;

    public UpdateMapParams(AddressDao addressDao,
                           IViewModelUIUpdates vmUIUpdate,
                           MapSettings mapSettings) {
        this.addressDao = addressDao;
        this.vmUIUpdate = vmUIUpdate;
        this.mapSettings = mapSettings;
    }
}