package com.test.android.skyhookdemo.Database;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import static androidx.room.OnConflictStrategy.IGNORE;
import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AddressDao {

    @Insert(onConflict = IGNORE)
    void Save(Address address);

    @Insert(onConflict = IGNORE)
    void UpdateMap(MapSettings settings);

    @Query("SELECT address FROM AddressTable")
    LiveData<List<String>> LiveLoadAllAddresses();

    @Query("SELECT address FROM AddressTable")
    List<String> LoadAllAddresses();

    @Query("SELECT * FROM MapsTable ORDER BY id DESC LIMIT 1")
    MapSettings LoadMapSettings();
}