package com.test.android.skyhookdemo.UI.Controls;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.android.skyhookdemo.R;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.AddressViewHolder> {
    private List<String> addressList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class AddressViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;
        public AddressViewHolder(TextView v) {
            super(v);
            textView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerAdapter(List<String> addressList) {
        this.addressList = addressList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent,
                                                int viewType) {
        // create a new view
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_view, parent, false);

        AddressViewHolder vh = new AddressViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {
        holder.textView.setText(addressList.get(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {

        if(addressList == null)
            return 0;

        return addressList.size();
    }
}