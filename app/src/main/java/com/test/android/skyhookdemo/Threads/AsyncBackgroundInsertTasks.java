package com.test.android.skyhookdemo.Threads;

import android.os.AsyncTask;

import com.test.android.skyhookdemo.Database.AddressDao;
import com.test.android.skyhookdemo.Models.Reponses.REGOResponse;
import com.test.android.skyhookdemo.Models.Reponses.REGOResponseAdapter;
import com.test.android.skyhookdemo.UI.MainMap.IViewModelUIUpdates;


public class AsyncBackgroundInsertTasks extends AsyncTask<UpdateTaskParams,Void, UpdateTaskParams> {

    protected UpdateTaskParams doInBackground(UpdateTaskParams...params) {
        AddressDao addressDao = params[0].addressDao;
        REGOResponse regoResponse = params[0].regoResponse;

        if(regoResponse != null)
        {
            addressDao.Save(REGOResponseAdapter.REGOResponseToAddress(regoResponse));
        }
        else
        {
            addressDao.Save(REGOResponseAdapter.FailedResponseToAddress());
        }

        return params[0];
    }

    protected void onProgressUpdate(Integer... progress)
    {

    }

    protected void onPostExecute(UpdateTaskParams params)
    {
        IViewModelUIUpdates callStatus = params.vmUIUpdate;
        params.vmUIUpdate.DecrementCivicCount();
    }
}