package com.test.android.skyhookdemo.Threads;

import android.os.AsyncTask;

import com.test.android.skyhookdemo.Database.AddressDao;
import com.test.android.skyhookdemo.Database.MapSettings;

public class AsyncMapBackgroundInsertTasks extends AsyncTask<UpdateMapParams,Void, Void> {

    protected Void doInBackground(UpdateMapParams... params) {

        AddressDao addressDao = params[0].addressDao;
        MapSettings settings = params[0].mapSettings;
        addressDao.UpdateMap(settings);

        return null;
    }

    protected void onProgressUpdate(Integer... progress) {

    }

    protected void onPostExecute(UpdateMapParams params) {
    }
}