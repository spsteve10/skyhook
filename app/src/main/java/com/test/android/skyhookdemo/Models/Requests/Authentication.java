package com.test.android.skyhookdemo.Models.Requests;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "authentication", strict = false)
public class Authentication {

    @Attribute(name = "version")
    private String version = "2.2";

    @Element(name = "key")
    private Key key = new Key();

}
