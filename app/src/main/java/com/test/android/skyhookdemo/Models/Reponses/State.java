package com.test.android.skyhookdemo.Models.Reponses;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "state", strict = false)
public class State {

    @Attribute(name = "code",required = false)
    public String code;
}
