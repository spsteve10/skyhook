package com.test.android.skyhookdemo.Models.Reponses;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "country", strict = false)
public class Country {

    @Attribute(name = "code",required = false)
    private String code;
}
