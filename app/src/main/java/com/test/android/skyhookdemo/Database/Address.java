package com.test.android.skyhookdemo.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "AddressTable")
public class Address {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "address")
    public String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }
}