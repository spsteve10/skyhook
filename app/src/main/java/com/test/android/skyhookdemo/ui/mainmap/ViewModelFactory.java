package com.test.android.skyhookdemo.UI.MainMap;

import com.test.android.skyhookdemo.Database.AddressRepository;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ViewModelFactory implements ViewModelProvider.Factory {
    private AddressRepository addressRepo;

    public ViewModelFactory(AddressRepository addressRepo) {
        this.addressRepo = addressRepo;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new MainMapViewModel(addressRepo);
    }
}
