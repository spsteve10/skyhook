package com.test.android.skyhookdemo.Threads;

import com.test.android.skyhookdemo.Database.AddressDao;
import com.test.android.skyhookdemo.Models.Reponses.REGOResponse;
import com.test.android.skyhookdemo.UI.MainMap.IViewModelUIUpdates;

public class UpdateTaskParams {
    AddressDao addressDao;
    IViewModelUIUpdates vmUIUpdate;
    REGOResponse regoResponse;

    public UpdateTaskParams(AddressDao addressDao,
                            IViewModelUIUpdates vmUIUpdate,
                            REGOResponse regoResponse) {
       this.addressDao = addressDao;
       this.vmUIUpdate = vmUIUpdate;
       this.regoResponse = regoResponse;
    }
}