package com.test.android.skyhookdemo.Models.Requests;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "key", strict = false)
public class Key {

    @Attribute(name = "key")
    private String key = "eJwVwUsKACAIBcB1hxH88LS2il0quns0I0P4g08bx3gVHEWFDZJIUKYLmXrAWoub7wMPMgr_";

    @Attribute(name = "username")
    private String username = "interview-candidate";

}
