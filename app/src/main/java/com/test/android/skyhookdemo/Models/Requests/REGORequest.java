package com.test.android.skyhookdemo.Models.Requests;

import org.simpleframework.xml.*;

@Root(name = "ReverseGeoRQ", strict = false)
public class REGORequest {

    @Attribute(name = "xmlns")
    private String xmlns = "http://skyhookwireless.com/wps/2005";

    @Attribute(name = "version")
    private String version = "2.8";

    @Attribute(name = "street-address-lookup")
    private String streetAddressLookup = "full";

    @Element(name = "authentication")
    private Authentication authentication = new Authentication();

    @Element(name = "point")
    public Point point = new Point();


}

