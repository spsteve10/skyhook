package com.test.android.skyhookdemo.Models.Reponses;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ReverseGeoRS", strict = false)
public class REGOResponse {

    @Attribute(name = "xmlns", required = false)
    private String xmlns = "";

    @Attribute(name = "version")
    private String version;

    @Element(name = "street-address",required = false)
    public StreetAddress streetAddress = new StreetAddress();
}

