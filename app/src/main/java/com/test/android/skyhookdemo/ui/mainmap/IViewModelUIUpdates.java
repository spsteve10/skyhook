package com.test.android.skyhookdemo.UI.MainMap;

import com.test.android.skyhookdemo.Database.MapSettings;

import java.util.List;

public interface IViewModelUIUpdates {
    void IncrementCivicCount();
    void DecrementCivicCount();
    void SetCivicList(List<String> civicList);
    void SetMapSettings(MapSettings mapSettings);
}
