package com.test.android.skyhookdemo.Threads;

import android.os.AsyncTask;

import com.test.android.skyhookdemo.Database.AddressDao;
import com.test.android.skyhookdemo.UI.MainMap.IViewModelUIUpdates;

import java.util.List;

public class AsyncBackgroundQueryTasks extends AsyncTask<UpdateTaskParams,Void, UpdateUIParams> {

    protected UpdateUIParams doInBackground(UpdateTaskParams... params) {
        AddressDao addressDao = params[0].addressDao;
        IViewModelUIUpdates vmUIUpdate = params[0].vmUIUpdate;

        List<String> addresses =  addressDao.LoadAllAddresses();
        UpdateUIParams uiParams = new UpdateUIParams(vmUIUpdate,addresses);
        return uiParams;
    }

    protected void onProgressUpdate(Integer... progress) {

    }

    protected void onPostExecute(UpdateUIParams params) {
        params.vmUIUpdate.SetCivicList(params.list);
    }
}