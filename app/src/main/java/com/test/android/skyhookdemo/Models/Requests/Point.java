package com.test.android.skyhookdemo.Models.Requests;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "point", strict = false)
public class Point {

    @Element(name = "latitude")
    public double latitude = 40.070939;

    @Element(name = "longitude")
    public double longitude = -75.464425;

}