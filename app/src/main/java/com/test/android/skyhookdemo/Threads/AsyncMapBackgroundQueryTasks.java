package com.test.android.skyhookdemo.Threads;

import android.os.AsyncTask;
import android.util.Log;

import com.test.android.skyhookdemo.Database.AddressDao;
import com.test.android.skyhookdemo.Database.MapSettings;
import com.test.android.skyhookdemo.UI.MainMap.IViewModelUIUpdates;

public class AsyncMapBackgroundQueryTasks extends AsyncTask<UpdateMapParams,Void, UpdateMapParams> {

    protected UpdateMapParams doInBackground(UpdateMapParams... params) {

        Log.d("MapSettings","Set mapSettings ");
        AddressDao addressDao = params[0].addressDao;
        IViewModelUIUpdates vmUIUpdate = params[0].vmUIUpdate;

        //make db query on background thread
        MapSettings mapSettings = addressDao.LoadMapSettings();

        UpdateMapParams uiParams = new UpdateMapParams(addressDao, vmUIUpdate, mapSettings);

        return uiParams;
    }

    protected void onProgressUpdate(Integer... progress) {

    }

    protected void onPostExecute(UpdateMapParams params) {
        //set map settings on main thread
        params.vmUIUpdate.SetMapSettings(params.mapSettings);
    }
}
