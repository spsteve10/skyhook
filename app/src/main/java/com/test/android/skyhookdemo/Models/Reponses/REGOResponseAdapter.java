package com.test.android.skyhookdemo.Models.Reponses;

import com.test.android.skyhookdemo.Database.Address;

public class REGOResponseAdapter
{
    public static String REGOResponseToString(REGOResponse regoResponse)
    {
        return "*" + regoResponse.streetAddress.streetNumber + " " +
                regoResponse.streetAddress.addressLine + " " +
                regoResponse.streetAddress.city + " " +
                regoResponse.streetAddress.state.code;
    }

    public static Address REGOResponseToAddress(REGOResponse regoResponse)
    {
        Address address = new Address();
        address.address = "*" + regoResponse.streetAddress.streetNumber + " " +
                regoResponse.streetAddress.addressLine + " " +
                regoResponse.streetAddress.city + " " +
                regoResponse.streetAddress.state.code;
        return address;
    }

    public static Address FailedResponseToAddress()
    {
        Address address = new Address();
        address.address ="Error:No Address";
        return address;
    }

}