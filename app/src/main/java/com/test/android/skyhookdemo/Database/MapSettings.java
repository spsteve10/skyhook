package com.test.android.skyhookdemo.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "MapsTable")
public class MapSettings {

    public MapSettings(double zoom, double lat, double lng)
    {
        this.zoom = zoom;
        this.lat = lat;
        this.lng = lng;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @ColumnInfo(name = "zoom")
    public double zoom;
    public double getZoom() { return this.zoom; }
    public void setZoom(double zoom) { this.zoom = zoom; }

    @ColumnInfo(name = "lat")
    public double lat;
    public double getlat() { return this.lat; }
    public void setLat(double lat) { this.lat = lat; }

    @ColumnInfo(name = "lng")
    public double lng;
    public double getLng() { return this.lng; }
    public void setLng(double lng) { this.lng = lng; }

}
