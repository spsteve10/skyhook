package com.test.android.skyhookdemo.Database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Address.class, MapSettings.class}, version = 3)
public abstract class AddressDatabase extends RoomDatabase {
    public abstract AddressDao addressDao();
}