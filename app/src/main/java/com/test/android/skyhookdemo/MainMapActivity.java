package com.test.android.skyhookdemo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.test.android.skyhookdemo.Database.AddressRepository;
import com.test.android.skyhookdemo.Database.AddressDatabase;
import com.test.android.skyhookdemo.Database.MapSettings;
import com.test.android.skyhookdemo.Threads.AsyncMapBackgroundInsertTasks;
import com.test.android.skyhookdemo.Threads.AsyncMapBackgroundQueryTasks;
import com.test.android.skyhookdemo.Threads.UpdateMapParams;
import com.test.android.skyhookdemo.UI.Controls.RecyclerAdapter;
import com.test.android.skyhookdemo.UI.MainMap.MainMapViewModel;
import com.test.android.skyhookdemo.UI.MainMap.ViewModelFactory;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;


public class MainMapActivity extends AppCompatActivity implements
        GoogleMap.OnCameraIdleListener,
        OnMapReadyCallback {

    private GoogleMap mMap;
    private MainMapViewModel model;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    AddressDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);

        //set map fragment
        if (savedInstanceState == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }


        db = Room.databaseBuilder(getApplicationContext(),
                AddressDatabase.class, "AddressDatabase").build();

        //instantiate view model
        //pass IRepository to Mock DB for testing
        model = ViewModelProviders.of(this, new ViewModelFactory(new AddressRepository(db.addressDao()))).get(MainMapViewModel.class);

        SetupRecyclerObserver();
        SetupCivicObserver();
        SetupMapObserver();
    }

    private void SetupMapObserver() {
        //Updating the map object moves the map
        model.GetMapSettings().observe(this, mapSettings -> {
            LatLng update = new LatLng(mapSettings.lat, mapSettings.lng);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(update));
            mMap.moveCamera(CameraUpdateFactory.zoomTo((float) mapSettings.zoom));
        });
    }

    private void SetupRecyclerObserver() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new RecyclerAdapter(model.CivicList.getValue());
        recyclerView.setAdapter(mAdapter);

        //updating the list updates the list
        model.CivicList.observe(this, addressList -> {
            // update UI
            mAdapter = new RecyclerAdapter(addressList);
            recyclerView.setAdapter(mAdapter);
            recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
        });
    }

    private void SetupCivicObserver() {
        // Create the observer which updates the call count entry.
        model.GetCivicCount().observe(this, civicCount -> {
            TextView et = ((TextView) findViewById(R.id.civicCountEditText));
            et.setText(civicCount.toString());
        });
    }

    public void onClickBtn(View v) {
        double lat = mMap.getCameraPosition().target.latitude;
        double lng = mMap.getCameraPosition().target.longitude;
        model.GetCivicForCoordinates(lat, lng);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnCameraIdleListener(this);

    }

    public void onCameraIdle() {

        double lat = mMap.getCameraPosition().target.latitude;
        double lng = mMap.getCameraPosition().target.longitude;
        double zoom = mMap.getCameraPosition().zoom;

        MapSettings settings = new MapSettings(zoom, lat, lng);

        //On idle, insert new location into DB on background thread
        UpdateMapParams params = new UpdateMapParams(db.addressDao(), model, settings);
        AsyncMapBackgroundInsertTasks myTask = new AsyncMapBackgroundInsertTasks();
        myTask.execute(params);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Load last idle location into map
        UpdateMapParams params = new UpdateMapParams(db.addressDao(), model, null);
        AsyncMapBackgroundQueryTasks myTask = new AsyncMapBackgroundQueryTasks();
        myTask.execute(params);
    }

}
