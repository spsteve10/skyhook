package com.test.android.skyhookdemo.UI.MainMap;


import com.test.android.skyhookdemo.Database.AddressRepository;
import com.test.android.skyhookdemo.Database.MapSettings;
import com.test.android.skyhookdemo.Models.Requests.REGORequest;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

public class MainMapViewModel extends ViewModel implements IViewModelUIUpdates {

    private AddressRepository addressRepo;
    private MutableLiveData<Integer> addressRequestingCount = new MutableLiveData<Integer>();
    public MutableLiveData<List<String>> civicList = new MutableLiveData();
    public MutableLiveData<MapSettings> mapSettings = new MutableLiveData();


    public MainMapViewModel(AddressRepository addressRepo)
    {
        this.addressRepo = addressRepo;
        this.addressRepo.RefreshList(this);
        addressRequestingCount.setValue(0);
    }

    public MutableLiveData<Integer> GetCivicCount()
    {
        return addressRequestingCount;
    }

    public void SetCivicCount(Integer addressRequestingCount)
    {
        this.addressRequestingCount.setValue(addressRequestingCount);
    }

    public final LiveData<List<String>> CivicList =
            Transformations.switchMap(civicList, (address) -> {
                return addressRepo.GetLiveAddresses();
            });

    public void SetCivicList(List<String> civicList)
    {
        this.civicList.setValue(civicList);
    }

    public LiveData<MapSettings> GetMapSettings() {
        return  mapSettings;
    }


    public void SetMapSettings(MapSettings mapSettings)
    {
        if(mapSettings == null)
        {
            this.mapSettings.setValue(new MapSettings(12,40.070939,-75.464425));
        }
        else
        {
            this.mapSettings.setValue(mapSettings);
        }
    }

    public void IncrementCivicCount()
    {
        SetCivicCount(GetCivicCount().getValue().intValue() + 1);
    }

    public void DecrementCivicCount()
    {
        if(GetCivicCount().getValue().intValue() > 0)
        {
            SetCivicCount(GetCivicCount().getValue().intValue() - 1);
        }
    }

    //web api call
    public void GetCivicForCoordinates(double lat, double lng)
    {
        try {
            REGORequest regoRequest = new REGORequest();
            regoRequest.point.latitude = lat;
            regoRequest.point.longitude = lng;

            addressRepo.GetAddresses(regoRequest, this);
        }
        catch (Exception e)
        {
            DecrementCivicCount();
        }
    }


}
