package com.test.android.skyhookdemo.Database;

import com.test.android.skyhookdemo.Models.Reponses.REGOResponse;
import com.test.android.skyhookdemo.Models.Requests.REGORequest;
import com.test.android.skyhookdemo.Networking.IWebservice;
import com.test.android.skyhookdemo.Networking.WebserviceSingleton;
import com.test.android.skyhookdemo.Threads.AsyncBackgroundInsertTasks;
import com.test.android.skyhookdemo.Threads.AsyncBackgroundQueryTasks;
import com.test.android.skyhookdemo.Threads.UpdateTaskParams;
import com.test.android.skyhookdemo.UI.MainMap.IViewModelUIUpdates;

import java.util.List;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressRepository {

    private final AddressDao addressDao;

    public AddressRepository(AddressDao addressDao) {
        this.addressDao = addressDao;
    }

    public LiveData<List<String>> GetLiveAddresses() {
        return addressDao.LiveLoadAllAddresses();
    }

    public void GetAddresses(REGORequest regoRequest, IViewModelUIUpdates callStatus) {
        RefreshAddress(regoRequest,callStatus);
    }

    private void RefreshAddress(final REGORequest regoRequest, IViewModelUIUpdates callStatus) {
        // Runs in a background thread.
        callStatus.IncrementCivicCount();

        WebserviceSingleton.GetWebServiceInstance().create(IWebservice.class).makeCivicRequest(regoRequest).
                    enqueue(new Callback<REGOResponse>(){
                        @Override
                        public void onResponse(Call<REGOResponse> call, Response<REGOResponse> response) {
                            UpdateTaskParams params = new UpdateTaskParams(addressDao, callStatus, response.body());
                            AsyncBackgroundInsertTasks myTask = new AsyncBackgroundInsertTasks();
                            myTask.execute(params);
                        }

                        @Override
                        public void onFailure(Call<REGOResponse> call, Throwable t) {

                            UpdateTaskParams params = new UpdateTaskParams(addressDao, callStatus, null);
                            AsyncBackgroundInsertTasks myTask = new AsyncBackgroundInsertTasks();
                            myTask.execute(params);
                        }
                    });
    }

    public void RefreshList(IViewModelUIUpdates callStatus)
    {
        UpdateTaskParams myParams = new UpdateTaskParams(addressDao, callStatus, null);
        AsyncBackgroundQueryTasks myTask = new AsyncBackgroundQueryTasks();
        myTask.execute(myParams);
    }

}
