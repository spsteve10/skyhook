package com.test.android.skyhookdemo.Networking;

import com.test.android.skyhookdemo.Models.Reponses.REGOResponse;
import com.test.android.skyhookdemo.Models.Requests.REGORequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IWebservice {
    /**
     * @POST declares an HTTP POST request
     * @Body REGORequest passes in a serialized REGORequest POJO
     */
    @Headers({
            "Content-Type: text/xml",
    })
    @POST("wps2/reverse-geo")
    Call<REGOResponse> makeCivicRequest(@Body REGORequest regoRequest);
}
