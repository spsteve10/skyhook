package com.test.android.skyhookdemo.Threads;

import com.test.android.skyhookdemo.UI.MainMap.IViewModelUIUpdates;

import java.util.List;

import androidx.lifecycle.LiveData;

public class UpdateUIParams {
    List<String> list;
    IViewModelUIUpdates vmUIUpdate;

    public UpdateUIParams(IViewModelUIUpdates vmUIUpdate,
                          List<String> list) {
        this.vmUIUpdate = vmUIUpdate;
        this.list = list;
    }
}