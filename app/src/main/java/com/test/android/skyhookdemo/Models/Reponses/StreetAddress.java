package com.test.android.skyhookdemo.Models.Reponses;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "street-address", strict = false)
public class StreetAddress {

    @Attribute(name = "distanceToPoint",required = false)
    public float distanceToPoint;

    @Element(name = "street-number",required = false)
    public int streetNumber;

    @Element(name = "address-line",required = false)
    public String addressLine;

    @Element(name = "city",required = false)
    public String city;

    @Element(name = "metro1",required = false)
    public String metroOne;

    @Element(name = "metro2",required = false)
    public String metroTwo;

    @Element(name = "postal-code",required = false)
    public String postalCode;

    @Element(name = "county",required = false)
    public String county;

    @Element(name = "state",required = false)
    public State state = new State();

    @Element(name = "country",required = false)
    public Country country = new Country();
}


//<ReverseGeoRS xmlns="http://skyhookwireless.com/wps/2005" version="2.26">
//<street-address distanceToPoint="28.27771331">
//<street-number>64</street-number>
//<address-line>Farnsworth St</address-line>
//<metro1>Boston</metro1>
//<metro2>Boston</metro2>
//<postal-code>02210</postal-code>
//<county>Suffolk</county>
//<state code="MA">Massachusetts</state>
//<country code="US">United States</country>
//</street-address>
//</ReverseGeoRS>
