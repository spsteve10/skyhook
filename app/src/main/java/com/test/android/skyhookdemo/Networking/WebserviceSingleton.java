package com.test.android.skyhookdemo.Networking;

import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class WebserviceSingleton {

    private static Retrofit retrofit;
    private static final String BASE_URL = "https://api.skyhookwireless.com:443/";

    /**
     * Create an instance of Retrofit object
     * */
    public static Retrofit GetWebServiceInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
