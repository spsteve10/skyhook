
# Skyhook
Sample android project for Skyhook.

#Android tools
This app was written in Java, AndroidX libraries, gradle toolset,
and both compile/target SDKs set to 28.

#Build info
Android Studio 3.3.2
Build #AI-182.5107.16.33.5314842, built on February 15, 2019
JRE: 1.8.0_152-release-1248-b01 x86_64
JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
macOS 10.14

#MVVM Pattern and Testability
This demo app was designed with the MVVM pattern for testability. The
viewmodel accepts a single IRepository object. This DI design allows a
mock database to be injected into the viewmodel object to validate unit tests.

In MVVM, unlike MVP, the viewmodel has no reference to the view. The
view utilizes the observer pattern to observe specific public data
members of the viewmodel. This loose coupling is why this pattern is
favored for testing.

#Core Libraries
Room Persistance Library used to persists data between restarts.

Retrofit Library for network calls.

Simple XML library for XML serial/de-serialization.

Lifecycle Library used to implement the MVVM viewmodel.  This enables 
the events that notify the view when an
observed data member has been updated.

Recycler view and view holder controls used in the listview for
memory optimization.

##Author
Sonny Stevenson